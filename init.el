;; Despliega reloj
(display-time-mode 1)

;; Cambia tamaño de fuente (descomentar de acuerdo a necesidades y fuentes presentes)
(set-face-attribute 'default nil :font "Hack-12")
(set-frame-font "Hack" nil t)

;; Incrementa el Garbage Collection para partir más rápido
(setq gc-cons-threshold (* 500 1024 1024))

;; No despliega mensaje inicial
(setq inhibit-startup-message t)

;; Agrega repositorios de paquetes
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(package-initialize)

;; Instala gestor de paquetes
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Instala Org-Mode si es que no está instalado
(unless (package-installed-p 'org)
  (package-refresh-contents)
  (package-install 'org))

;; Activa indentación por defecto (usar #+STARTUP: noindent para desactivar en un buffer)
;; (setq org-startup-indented t)
;; Org plus contrib
(unless (package-installed-p 'org-plus-contrib)
  (package-refresh-contents)
  (package-install 'org-plus-contrib))

;; Carga configuración hecha en Org-Mode
(org-babel-load-file (concat user-emacs-directory "config.org"))

;; Termina el Garbage Collector
(add-hook 'after-init-hook (lambda () (setq gc-cons-threshold (* 5 1024 1024))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("c856158cc996d52e2f48190b02f6b6f26b7a9abd5fea0c6ffca6740a1003b333" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default)))
 '(package-selected-packages
   (quote
    (esup ob-julia avy exec-path-from-shell dired-quick-sort spacemacs-theme elpa-elpy rainbow-delimiters elpy jedi org-bullets elmacro csv-mode vlf expand-region multiple-cursors yasnippet-snippets yasnippet web-mode use-package spaceline ox-twbs ox-reveal org-download ob-ipython ob-async neotree json-mode htmlize ess counsel babel))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)

;; Para nuevos frames
(set-default-font "Hack-12")
(setq default-frame-alist '((font . "Hack-12"))) 
